window.onload = function() {

    var game = new Phaser.Game(1300, 900, Phaser.AUTO, 'game', { preload: preload, create: create, update: update }, false, false);
    
    var SCALE = 4;
    var BRICK_ROWS = 7;
    var BRICK_COLUMNS = 9;

    var ball;
    var bricks;
    var paddle;
    var walls;

    var _brickCheckIndex;

    function preload () {

        game.load.image('ball', 'assets/ball.png');
        game.load.image('brick', 'assets/brick.png');
        game.load.image('paddle', 'assets/paddle.png');

    }

    function create () {

        // Settings
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;

        // Ball
        ball = game.add.sprite(game.width - 180, game.world.centerY + 40, 'ball');
        ball.anchor.set(0.5, 0.5);
        ball.scale.set(SCALE);
        game.physics.enable(ball, Phaser.Physics.ARCADE);
        ball.body.bounce.set(1);
        ball.body.collideWorldBounds = true;


        // Bricks
        bricks = new Array();
        for (var i = 0; i < BRICK_ROWS; i++) {
            for (var j = 0; j < BRICK_COLUMNS; j++) {
                bricks.push(game.add.sprite((j * 33 * SCALE) + 53, (i * 9 * SCALE) + 100, 'brick'));
                var currentBrick = bricks[(i * BRICK_COLUMNS) + j];
                currentBrick.scale.set(SCALE);
                game.physics.enable(currentBrick, Phaser.Physics.ARCADE);
                currentBrick.body.immovable = true;
            }
        }

        // Paddle
        paddle = game.add.sprite(game.world.centerX, game.height - 24, 'paddle');
        paddle.anchor.set(0.5, 0.5);
        paddle.scale.set(SCALE);
        game.physics.enable(paddle, Phaser.Physics.ARCADE);
        paddle.body.immovable = true;

        // Walls
        walls = new Array();
        walls.push(game.add.sprite());

        // Start game values
        ball.body.velocity.set(-1500, 1400);

    }

    function update() {

        // Check for ball colliding with bricks and paddle
        game.physics.arcade.collide(ball, paddle, null, onPaddleCollision);
        for (var i = 0; i < bricks.length; i++) {
            _brickCheckIndex = i;
            game.physics.arcade.collide(ball, bricks[i], undefined, onBrickCollision);
        }

    }

    function onPaddleCollision(sprite1, sprite2, index) {
        console.log("Paddle was hit!");
    }

    function onBrickCollision(sprite1, sprite2) {
        bricks[_brickCheckIndex].kill();
        bricks.splice(_brickCheckIndex, 1);
    }

};
